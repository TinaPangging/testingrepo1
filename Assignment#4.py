# -*- coding: utf-8 -*-
"""
Created on Sun Feb  4 16:33:31 2018

@author: think
"""

import numpy 
import matplotlib.pyplot as plt
import pyproj

# Define a function similar to range() function but support the float type
def frange(start, stop, step):
    i = start
    while i < stop-step:
        yield i
        i += step
        
# Define two projections
g_project = pyproj.Proj("+init=epsg:4326 +to +proj=gnom +lat_0=90 +lon_0=-45+x_0=0+y_0=0 +units=m")
s_project = pyproj.Proj("+init=epsg:2953 +to +proj=stere +lat_0=90 +lat_ts=75+lon_0=-45+x_0=0+y_0=0 +units=m")

# Calculate the position of the North Pole according two projections
(p1,p2)=(0,90)
(gp1,gp2)=g_project (p1,p2)
(sp1,sp2)=s_project (p1,p2)
northpole_g=gp2
northpole_s=sp2

# Create an empty array to storing the values of differences in distance from North Pole
difference = numpy.array([])
lat = numpy.arange(60.,90.,0.1)

# Calculate the defferences in distance from the North Pole according to Gnomonic and Stereographic projection
for i in frange(60.,90.,0.1):
    (x1,y1) = (0,i)
    (x2,y2) = g_project(x1,y1)
    (x3,y3) = s_project(x1,y1)
    g_northpole = gp2-y2
    s_northpole = sp2-y3
    difference = numpy.append(difference,g_northpole-s_northpole)
    
plt.plot(lat, difference, '-', )
plt.xlabel('Latitude (°N)')
plt.ylabel('Differences (m)')
plt.title('Differences in distance from the North Pole')
plt.show()


#g_lat_array = numpy.array([])
#s_lat_array = numpy.array([])
#for i in frange(60.,90.,0.1):
#    (x1,y1)=(0,i)
#    (x2,y2)=g_project(x1,y1)
#    g_lat = y2
#    g_lat_array = numpy.append(g_lat_array,y2)
#plt.plot(lat, g_lat_array, '-', )
#plt.show()
#
#for i in frange(60.,90.,0.1):
#    (x1,y1)=(-45,i)
#    (x2,y2)=s_project(x1,y1)
#    s_lat = y2
#    s_lat_array = numpy.append(s_lat_array,y2)
#plt.plot(lat, s_lat_array, '-', )
#plt.show()
